import win32com.client as win32
import os
from os import listdir
from os.path import isfile, join


def openWorkbook(xlapp, xlfile):
    try:        
        xlwb = xlapp.Workbooks(xlfile)            
    except Exception as e:
        try:
            xlwb = xlapp.Workbooks.Open(xlfile)
        except Exception as e:
            print(e)
            xlwb = None                    
    return(xlwb)

def exclToPDF(filename):
    try:
        excel = win32.gencache.EnsureDispatch('Excel.Application')
        wb = openWorkbook(excel, os.getcwd() + '\\' + filename)
        for ws in wb.Worksheets:
            ws.PageSetup.Zoom = False
            ws.PageSetup.FitToPagesTall = False
            ws.PageSetup.FitToPagesWide = 1
            ws.PageSetup.LeftMargin = 25
            ws.PageSetup.RightMargin = 25
            ws.PageSetup.TopMargin = 50
            ws.PageSetup.BottomMargin = 50
            ws.ExportAsFixedFormat(0, os.getcwd() + '\\' + filename + '_' + ws.Name + '.pdf')
        wb.Close(True)
    except Exception as ex:
        print(ex)
    finally:
        wb = None
        excel = None

def getAllExcelFiles():
    mypath = os.getcwd()
    files = [f for f in listdir(mypath) if isfile(join(mypath, f)) and  (f[-4:] == 'xlsx' or f[-3:] == 'xls')]
    print(files)

try:
    getAllExcelFiles()
    #print("Place the Excel File in the same Folder as this script")
    #filename = input("Enter the filename of the Excel File you want to convert to PDFs:")
    #exclToPDF(filename)
except Exception as e:
    print(e)

